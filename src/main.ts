import MessClass from './MessClass'
const main = async () => {
  const messObj = new MessClass()
  const mess: string = await messObj.getMess()
  console.log(mess)
  return mess
}

export default main
